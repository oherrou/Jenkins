__author__ = "Delta Dore"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Olivier Herrou"
__email__ = "oli.herrou@gmail.com"
__status__ = "Production"

"""  Custom Print library
  This library has been developed to customize output message with specific tag and indentation 

  Tested with the following python version:
   - Windows 7 32bits, Python V3.6.4
"""

"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------- IMPORT -------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""


"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------- GLOBAL -------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""



"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------ FUNCTIONS -----------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
def printError(iLevel, szMessage):
  """ 
   This function will print messages prefixed with an error TAG that can be used to track errors 
   or give advice to the system. TAGs can be accessed according to the value of the first 
   parameter 'iLevel'
   Parameters:
     - iLevel, the level that set the TAGs as followed 
      (0: DEBUG, 1: INFO, 2: WARN, 3: ERROR, 4: FATAL, 5: EXCEP, otherwise: UNKWN)
      - szMessage, the message to print, it can be formatted 
  """
  try:
    if(iLevel == 0):
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='DEBUG', fill='5', sp=' --- ',fill2='5' ,msg=szMessage, fill3='0'))
    elif(iLevel == 1):
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='INFO', fill='5', sp=' --- ', fill2='5' ,msg=szMessage, fill3='0'))
    elif(iLevel == 2):
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='WARN', fill='5', sp=' --- ', fill2='5' ,msg=szMessage, fill3='0'))
    elif(iLevel == 3):
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='ERROR', fill='5', sp=' --- ', fill2='5' ,msg=szMessage, fill3='0'))
    elif(iLevel == 4):
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='FATAL', fill='5', sp=' --- ', fill2='5' ,msg=szMessage, fill3='0'))
    elif(iLevel == 5):
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='EXCEP', fill='5', sp=' --- ', fill2='5' ,msg=szMessage, fill3='0'))
    else:
      print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx='UNKWN', fill='5',sp=' --- ',fill2='5',msg=szMessage,fill3='0'))
  except Exception as e:
    print("Error while using the function 'printError' of the library CustomPrint")
    print("Exception is {}".format(e))


def printMessage(iSpace, szMessage):
  """
   This function will print a message with spaces in front of it according to its parameters
   Parameters:
     - iSpace, the number of space
     - szMessage, the message to print, it can be formatted
  """
  try:
    print("{sp: >{fill}}{msg: <{fill2}}".format(sp='', fill=iSpace, msg=szMessage, fill2='0'))
  except Exception as e:
    print("Error while using the function 'printMessage' of the library CustomPrint")
    print("Exception is {}".format(e))


def printPrefix(iSpacePfx, szPfx, iSpaceMsg, szMsg):
  """
   This function will print a message with spaces in front of a custom prefix, then spaces and the
   messages
   Parameters:
     - iSpace, the number of space
     - szMessage, the message to print, it can be formatted
  """
  try:
    print("[{pfx: >{fill}}]{sp: <{fill2}}{msg: >{fill3}}".format(pfx=szPfx, fill=iSpacePfx, sp='', fill2=iSpaceMsg, msg=szMsg, fill3='0'))
  except Exception as e:
    print("Error while using the function 'cprint_Prefix' of the library CustomPrint")
    print("Exception is {}".format(e))


"""----------------------------------------------------------------------------------------------"""
"""-------------------------------------------- MAIN --------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
if __name__ == '__main__':
	"""
		This function can be called to test the library

		Messages will be printed in the console output:
		 - one using this library function
		 - one using the default print function of python
		Both messages should be identical, if it's not the case it means the library should be checked
		for any regression
	"""
  try:
    print("---------- Testing the Custom Print Library ----------\n\n\n")

    """ Function printMessage """
    print("\n")
    printPrefix(10, "TEST", 3, "Testing {} function".format("printMessage"))
    print("[      TEST]   Testing printMessage function")

    printPrefix(3, "TEST", 10, "Testing {} function".format("printMessage"))
    print("[TEST]          Testing printMessage function")

    """ Function printPrefix """
    print("\n")
    printMessage(0, "Testing {} function".format("printPrefix"))
    print("Testing printPrefix function")
    printMessage(10, "Testing {} function".format("printPrefix"))
    print("          Testing printPrefix function")

    """ Function printError """
    print("\n")
    printError(0, "This is a debug message: {}".format("debug message"))
    print("[DEBUG] --- This is a debug message: debug message")

    printError(1, "This is an info message: {}".format("info message"))
    print("[ INFO] --- This is an info message: info message")

    printError(2, "This is a warning message: {}".format("warning message"))
    print("[ WARN] --- This is a warning message: warning message")

    printError(3, "This is an error message: {}".format("error message"))
    print("[ERROR] --- This is an error message: error message")

    printError(4, "This is a fatal error message: {}".format("fatal error message"))
    print("[FATAL] --- This is a fatal error message: fatal error message")

    printError(5, "This is an exception message: {}".format("exception message"))
    print("[EXCEP] --- This is an exception message: exception message")

    printError(6, "This is an unknown message: {}".format("unknown message"))
    print("[UNKWN] --- This is an unknown message: unknown message")

    print("\n\n\n\nEvery message should be duplicate and be exactly the same, if not test is considered as failed\n\n")

  except Exception as e:
    print("Error while testing the CustomPrint Library")
    print("Exception is {}".format(e))
