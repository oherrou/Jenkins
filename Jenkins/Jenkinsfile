import org.json.*

/*
 Il va etre nécessaire de récupérer les informations suivantes:
  - Nombre de job passé
  - Nombre de job skip
  - Nombre de job échoué
  - Timestamp
*/

//http://localhost:8080/job/Jenkins/job/master/301/input/Deployment/abort
//http://localhost:8080/job/Jenkins/job/master/301/wfapi/inputSubmit?inputId=Deployment


/* CUSTOM */
def szGitUrl = "https://gitlab.com/oherrou/Jenkins.git"
def szBuildBat = "Jenkins/Scripts/build.bat"
def szDoxygenBat = "Jenkins/Scripts/doxygen.bat"
def szTestBat = "Jenkins/Scripts/xxx"
def szCodingRulesBat = "Jenkins/Scripts/xxx"
def szStaticAnalysisBat = "Jenkins/Scripts/xxx"
def szSonarQubeBat = "Jenkins/Scripts/xxx"

/*
 Definition of job status
 0: Success/not called
 1: Failed
 2: Success with warning
 3: Skipped
*/
def iStatusCheckout = 0
def iStatusBuild = 0
def iStatusTest = 0
def iStatusCodingRules = 0
def iStatusStaticAnalysis = 0
def iStatusDoxygen = 0
def iStatusSonarQube = 0
def iStatusRelease = 0

def slackNotifyBuildFailed(String szJob)
{
 // Variable
  def szAuthor = bat(returnStdout: true, script: "@echo off\n git show -s --format=%%an").trim()
  def szCommitMessage = bat(returnStdout: true, script: '@echo off\n git log -1 --pretty=%%s').trim()

  // Create JSON for attachment, more information on slack API
  JSONObject joSection = new JSONObject()
  joSection.put('color',"danger")
  joSection.put('author_name', "${szAuthor}")
  joSection.put('author_icon', "https://i.imgur.com/bKMYk1B.png")
  joSection.put('title', "Pipeline failed")
  joSection.put('title_link', "${env.RUN_DISPLAY_URL}")
  joSection.put("footer", "Jenkins Bot")
  joSection.put("footer_icon", "https://i.imgur.com/lbC4ySi.png")
  joSection.put("ts", "12345678") // TODO Handle timestamp
  joSection.put("fallback", "Pipeline failed: ${szJob}")

  JSONObject joField1 = new JSONObject()
  joField1.put("title", "Branch")
  joField1.put("value", "${env.BRANCH_NAME}")
  joField1.put("short", "true")

  JSONObject joField2 = new JSONObject()  
  joField2.put("title", "Last commit")
  joField2.put("value", "${szCommitMessage}")
  joField2.put("short", "true")

  JSONObject joField3 = new JSONObject()  
  joField3.put("title", "Job")
  joField3.put("value", "${szJob} ")
  joField3.put("short", "true")


  JSONArray jaField = new JSONArray()
  jaField.put(joField1)
  jaField.put(joField2)
  jaField.put(joField3)

  joSection.put("fields", jaField)

  JSONArray myAttachments = new JSONArray()
  myAttachments.put(joSection)
  myAttachments.put(jaField)
  println(myAttachments.toString())

  // Send notifications
  try
  {
    slackSend(token: "TCFbQ8VJ2Ni3Zc2eiALmL9BS",
            baseUrl: "https://ddtestgroupe.slack.com/services/hooks/jenkins-ci/",
            attachments: myAttachments.toString())     
  }
  catch(Exception e)
  {
    println(e)
  }    
}

def slackNotifyDeployment()
{
  // Variable
  def szAuthor = bat(returnStdout: true, script: "@echo off\n git show -s --format=%%an").trim()
  def szCommitMessage = bat(returnStdout: true, script: '@echo off\n git log -1 --pretty=%%s').trim()
  def szJobName = "${env.JOB_NAME}".split('/').first()
  def szUrlProceed = "${JENKINS_URL}job/${szJobName}/job/${env.BRANCH_NAME}/${BUILD_ID}/wfapi/inputSubmit?inputId=Deployment"
  def szUrlCancel = "${JENKINS_URL}job/${szJobName}/job/${env.BRANCH_NAME}/${BUILD_ID}/input/Deployment/abort"

  println("${szUrlCancel}")
  println("${szUrlProceed}")

  // Create JSON for attachment, more information on slack API
  JSONObject joSection = new JSONObject()
  joSection.put('color',"good") // TODO Get correct color according to pipeline status
  joSection.put('author_name', "${szAuthor}")
  joSection.put('author_icon', "https://i.imgur.com/bKMYk1B.png")
  joSection.put('title', "Job '${JOB_NAME}' (${BUILD_NUMBER}) is waiting for input")
  joSection.put('title_link', "${env.RUN_DISPLAY_URL}")
  joSection.put("footer", "Jenkins Bot")
  joSection.put("footer_icon", "https://i.imgur.com/lbC4ySi.png")
  joSection.put("ts", "12345678") // TODO Handle timestamp
  joSection.put("fallback", "Job '${JOB_NAME}' (${BUILD_NUMBER}) is waiting for input")
  
  // Fields
  JSONObject joField1 = new JSONObject()
  joField1.put("title", "Branch")
  joField1.put("value", "${env.BRANCH_NAME}")
  joField1.put("short", "true")

  JSONObject joField2 = new JSONObject()  
  joField2.put("title", "Last commit")
  joField2.put("value", "${szCommitMessage}")
  joField2.put("short", "true")

  JSONObject joField3 = new JSONObject()  
  joField3.put("title", "Test Results")
  joField3.put("value", "Passed: 10, Failed: 0, Skipped:2") // Todo
  joField3.put("short", "true")

  JSONArray jaField = new JSONArray()
  jaField.put(joField1)
  jaField.put(joField2)
  jaField.put(joField3)

  joSection.put("fields", jaField)

  // Button
  JSONObject joButton1 = new JSONObject()  
  joButton1.put("type", "button")
  joButton1.put("name", "Proceed deployment")
  joButton1.put("text", "Proceed deployment")
  joButton1.put("url", "${szUrlProceed}")
  joButton1.put("style", "primary")

  JSONObject joButton2 = new JSONObject()  
  joButton2.put("type", "button")
  joButton2.put("name", "Cancel deployment")
  joButton2.put("text", "Cancel deployment")
  joButton2.put("url", "${szUrlCancel}")
  joButton2.put("style", "danger")

  JSONArray jaButton = new JSONArray()
  jaButton.put(joButton1)
  jaButton.put(joButton2)

  joSection.put("actions", jaButton)


  JSONArray myAttachments = new JSONArray()
  myAttachments.put(joSection)
  myAttachments.put(jaField)
  println(myAttachments.toString())

  // Send notifications
  try
  {
    slackSend(token: "TCFbQ8VJ2Ni3Zc2eiALmL9BS",
            baseUrl: "https://ddtestgroupe.slack.com/services/hooks/jenkins-ci/",
            attachments: myAttachments.toString())     
  }
  catch(Exception e)
  {
    println(e)
  }    
}

def jobCheckout(String szUrl)
{
  try
  {
    git(url: szUrl)
  }
  catch (Exception e)
  {
    println(e)
    iStatusCheckout = 1
    error("Exception")
    slackNotifyBuildFailed("Checkout")
  }
  
}

def jobBuild(String szBat)
{
  try
  {
    iStatusBuild = bat(returnStatus: true, script: szBat)
  }
  catch (Exception e)
  {
    println(e)
    iStatusBuild = 1
    error("Exception")
    slackNotifyBuildFailed("Build")
  }

  // Check output status
  if (iStatusBuild == 0) // Success
  {
    // Success, do nothing
  }
  else if (iStatusBuild == 1) // Erros
  {
    println("Build failed due to errors in the job [BUILD]")
    slackNotifyBuildFailed("Build")
    error("")
  }
  else if (iStatusBuild == 2) // Success with warnings 
  {
    //TODO set build as unstable if possible but not handle in python script yet
  }
}

def jobDoxygen(String szBat)
{
  try
  {
    iStatusDoxygen = bat(returnStatus: true, script: szBat)
  }
  catch (Exception e)
  {
    println(e)
    iStatusDoxygen = 1
    error("Exception")
  }
}

def jobTest(String szBat)
{
  println("TODO")
}

def jobCodingRules(String szBat)
{
  println("TODO")
}

def jobStaticAnalysis(String szBat)
{
  println("TODO")
}

def jobSonarQube(String szBat)
{
  println("TODO")
}


pipeline 
{
  agent any

  options
  {    
    timeout(time: 10, unit: 'MINUTES')
  }

  stages
  {
    /* PREBUILD */
    stage ('Checkout')
    {
      steps
      {
        script
        {
          jobCheckout(szGitUrl)
        }
      }
    }
    
    /* BUILDING */
    stage ('Build')
    {
      steps
      {
        script
        {
          jobBuild(szBuildBat)
        }
      }
    }
    
    /* TEST */
    stage ('Test')
    {
      steps
      {
        script
        {
          jobTest(szTestBat)
        }
      }
    }

    /* ANALYSE */
    stage ("Analyse")
    {
      parallel
      {
        stage ('Doxygen')
        {          
          steps
          {
            script
            {
              jobDoxygen(szDoxygenBat)
            }
          }
        }
        
        stage ('Coding rules')
        {
          steps
          {
            script
            {
              jobCodingRules(szCodingRulesBat)
            }
          }
        }

        stage ('Static analysis')
        {
          steps
          {
            script
            {
              jobStaticAnalysis(szStaticAnalysisBat)
            }
          }
        }
      }
    }
    
    /* DEPLOYMENT */    
    stage ('SonarQube')
    {
      steps
      {
        script
        {
          jobSonarQube(szSonarQubeBat)
        }
      }
    }

    stage ('Release')
    {
      steps
      {
        slackNotifyDeployment()
        timeout(time: 10, unit: 'MINUTES')
        {
          input(message: 'Deploy to Production?', id: "Deployment") 
        }
        
      }
    }
  }
}